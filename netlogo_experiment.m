%   defaults are
%       - meander-length 85
%       - max-turning-angle 1.5
%       - river-width 7.5
%
%       - evaporation-rate 1.8
%       - transfer-rate 76
%
%       - airborne-spread 10
%       - neighborhood-spread 65
%
%   	- flower-lower-limit 30
%       - flower-upper-limit 100
%   	- max-age 7
% ------------------------------------------

%   - max turning angle
%       parameter_index = 3, parameter_variation = 18, n_runs = 3,
%       axis_range = [0 1.7 0 1], range = (0:0.1:1.7), name = "variying-max-turning-angle"

%   - meander length 
%       parameter_index = 2, parameter_variation = 17, n_runs = 3,
%       axis_range = [50 210 0 1], range = (50:10:210), name = "variying-meander-length"

%   - evaporation-rate
%        parameter_index = 11, parameter_variation = 30, n_runs = 3,
%        axis_range = [1 30 0 1], range = (1:1:30), name = "variying-evaporation-rate"

%   - transfer-rate
%        parameter_index = 6, parameter_variation = 26, n_runs = 3,
%        axis_range = [40 90 0 1], range = (40:2:90), name = "variying-transfer-rate"

%   - airborne-spread
%        parameter_index = 7, parameter_variation = 20, n_runs = 5,
%        axis_range = [1 96 0 1], range = (1:5:96), name = "variying-airborne-spread"

%   - neighborhood-spread
%        parameter_index = 8, parameter_variation = 20, n_runs = 5,
%        axis_range = [1 96 0 1], range = (1:5:96), name = "variying-neighborhood-spread"

%   - max-age
%        parameter_index = 11, parameter_variation = 15, n_runs = 3,
%        axis_range = [1 29 0 1], range = (1:2:29), name = "variying-max-age"

parameter_index = 7;
parameter_variation = 20; % (number of runs) / n_runs
n_runs = 5;
range = (1:5:96);
axis_range = [1 96 0 1];
name = "variying-airborne-spread";

figure_file_format = "svg";

data_mat = data{:, [1,parameter_index,12,13,14,15]}; % 1 is run number, parameter_index, 12 is step, 13 - 15 are flower counts

use_runs = n_runs; % n_runs default, must be smaller or equal to n_runs
start_with_run = 0; % 0 default, start_with_run < n_runs must be true
% and start_with_run + use_runs <= n_runs

max_step = 1000;
flowers = 3;
flower_1_index = 4;

significant_from = 800;

avg_data = [];
index = 1;

% for each value of parameter process in bulk of n runs
for i=1:n_runs:(n_runs * parameter_variation - 1)
    current_data = [diag(eye(max_step + 1)*index), (0:max_step)'];
    for k=0:(flowers-1)
        current_mean_data = [];
       
        % average data in n runs for each value of parameter
        for j=start_with_run:((start_with_run + use_runs) - 1)
            current_mean_data = [current_mean_data, data_mat(data_mat(:,1) == (i + j), flower_1_index + k)];
        end
        current_data = [current_data, mean(current_mean_data, 2)];
    end
    avg_data = [avg_data; current_data];
    index = index + 1;
end

long_avg = [];
percentage = [];
max_percentage_distance = [];
for i=1:parameter_variation
    current_mean = [];
    for k=0:(flowers-1)
        current_mean = [current_mean, mean(avg_data(avg_data(:,1) == i & avg_data(:,2) > significant_from, 3 + k))];
    end
        
    long_avg = [long_avg; current_mean];
    
    current_sum = sum(current_mean);
    current_percentage = current_mean / current_sum;
    percentage = [percentage; current_percentage];
    max_percentage_distance = [max_percentage_distance; max(current_percentage) - min(current_percentage)];
end

% part of filename indiciting runs used and significancy
filename_runs = "runs-" + start_with_run + "->" + use_runs;
filename_significant_from = "significant-from-" + significant_from;

fig = figure;
hold on
plot(range, long_avg(:,1))
plot(range, long_avg(:,2), 'r')
plot(range, long_avg(:,3), 'g')

saveas(fig, name + "_" + "avg-values" + "_" + filename_runs + "_" + filename_significant_from + "." + figure_file_format);


% Graph of percentages
fig = figure;
axis(axis_range)
hold on
area(range, percentage(:,1) + percentage(:,2) + percentage(:,3))
area(range, percentage(:,1) + percentage(:,2))
area(range, percentage(:,2))

saveas(fig, name + "_" + "percentages" + "_" + filename_runs + "_" + filename_significant_from + "." + figure_file_format);

fig = figure;
% axis(axis_range)
hold on
plot(range, max_percentage_distance);

saveas(fig, name + "_" + "max-percentage-distance" + "_" + filename_runs + "_" + filename_significant_from + "." + figure_file_format);
